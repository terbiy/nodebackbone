var express = require('express');
var router = express.Router();
var stream = require('../own_modules/twitter');

/* GET tweet updates. */
router.get('/', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");

  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Connection': 'keep-alive',
    'Cache-Control': 'no-cache'
  });

  // Высылаем данные твита.
  stream.on('data', function(tweet) {
    // console.log(tweet);
    console.log(tweet.user.name);
    console.log(tweet.text);
    res.write(
      "data: " +
      JSON.stringify({
        username: tweet.user.name,
        text: tweet.text
      }) +
      "\n\n"
    );
  });
});

module.exports = router;
