// Соединение с сервером для получения новых твитов.
var app = app || {};

(function () {
  var twitterStream = new EventSource('/tweets');

  // twitterStream.addEventListener('message', function(e) {
  //   tweet = JSON.parse(e.data);
  //   console.log(tweet.text);
  // }, false);

  twitterStream.addEventListener('open', function(e) {
    console.log("Установлено соединение с сервером.");
  }, false);

  twitterStream.addEventListener('error', function(e) {
    if (e.readyState == EventSource.CLOSED) {
      console.log("Произошла ошибка.");
    }
  }, false);

  app.twitterStream = twitterStream;

})();