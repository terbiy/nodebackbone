// Модель твита.
var app = app || {};

(function () {
  'use strict';

  app.Tweet = Backbone.Model.extend({
    defaults: {
      name: "",
      text: ""
    }
  });

  // Так как данные подгружаются не через Collection, а через EventSource,
  // то мне необходимо избавиться от поля url в коллекции. Источник способа:
  // http://stackoverflow.com/a/18347250/2008562.
  var getNull = function () { return null; };

  app.Tweet.prototype.sync = getNull;
  app.Tweet.prototype.fetch = getNull;
  app.Tweet.prototype.save = getNull;
})();
