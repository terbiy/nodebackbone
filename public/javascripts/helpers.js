var helpers = helpers || {};

(function () {
  // Excellent solution for Backbone to listen to not Backbone events:
  // http://stackoverflow.com/a/17472399/2008562
  /**
   * Use Backbone Events listenTo/stopListening with any DOM element
   *
   * @param {DOM Element}
   * @return {Backbone Events style object}
   **/
  helpers.asEvents = function (el) {
    var args;
    return {
        on: function(event, handler) {
            if (args) throw new Error("this is one off wrapper");
            el.addEventListener(event, handler, false);
            args = [event, handler];
        },
        off: function() {
            el.removeEventListener.apply(el, args);
        }

    };
  }
})();