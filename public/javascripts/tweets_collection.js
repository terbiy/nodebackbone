var app = app || {};

(function () {
  'use strict';

  // Коллекция поступающих твитов.
  var Tweets = Backbone.Collection.extend({
    model: app.Tweet,
    initialize: function () {
      // Хотелось бы переписать данную часть через listenTo, но указанный
      // в комментарии метод устарел.
      // var backboneCompatibleStream = helpers.asEvents(app.twitterStream);
      // this.listenTo(backboneCompatibleStream, "message", this.addOne);
      app.twitterStream.addEventListener('message', this.addOne, false);
    },

    // Здесь мне не нужен url, так как данные поступают из EventSource.
    // url: "",

    // Функция для обработки поступающих твитов.
    addOne: function (event) {
      var tweet = JSON.parse(event.data);
      app.tweets.create(tweet);
    }

  });

  // Создаём коллекцию твитов.
  app.tweets = new Tweets();
})();