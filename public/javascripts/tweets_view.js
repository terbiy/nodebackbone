/*global Backbone, jQuery, _ */
var app = app || {};

(function ($) {
  'use strict';

  app.AppView = Backbone.View.extend({

    // Создаём твиты в уже существующем на странице элементе.
    el: '#tweets',

    // At initialization we bind to the relevant events on the `tweets`
    // collection, when items are added or changed. Kick things off by
    // loading any preexisting tweets that might be saved in *localStorage*.
    initialize: function () {
      this.listenTo(app.tweets, 'add', this.addOne);
      this.listenTo(app.tweets, 'reset', this.addAll);

      // Мне пока не очень ясно предназначение данного кода, выглядит важным.
      // Suppresses 'add' events with {reset: true} and prevents the app view
      // from being re-rendered for every model. Only renders when the 'reset'
      // event is triggered at the end of the fetch.
      // app.tweets.fetch({reset: true});
    },

    render: function () {
    },

    // Функция для добавления единственного твита.
    addOne: function (tweet) {
      var view = new app.TweetView({
        model: tweet
      });
      this.$el.prepend(view.render().el);
    },

    // Add all items in the **tweets** collection at once.
    addAll: function () {
      this.$list.html('');
      app.tweets.each(this.addOne, this);
    }
  });
})(jQuery);