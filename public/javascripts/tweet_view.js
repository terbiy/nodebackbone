/*global Backbone, jQuery, _ */
var app = app || {};

(function ($) {
  'use strict';

  // Вид для поступающего твита.
  app.TweetView = Backbone.View.extend({
    tagName:  'li',
    className: "tweet",

    // Функция для применения шаблона твита.
    template: _.template($('#tweet-template').html()),

    // DOM-события, обрабатываемые твитом.
    events: {
    },

    initialize: function () {
      this.listenTo(this.model, 'change', this.render);
    },

    render: function () {
      var element = this.template(this.model.toJSON());
      this.$el.html(element);
      return this;
    }
  });
})(jQuery);